FactoryBot.define do
    factory :user do
      nombre { Faker::Name.nombre }
      email 'foo@bar.com'
      password 'foobar'
    end
  end