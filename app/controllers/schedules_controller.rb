class SchedulesController < ApplicationController
    before_action :set_schedule, only: [:show, :update, :destroy]

  # GET /todos
  def index
    @schedule = Schedule.all
    json_response(@schedule)
  end

  # POST /todos
  def create
    @schedule = Schedule.create!(schedule_params)
    json_response(@schedule, :created)
  end

  # GET /todos/:id
  def show
    json_response(@schedule)
  end

  # PUT /todos/:id
  def update
    @schedule.update(schedule_params)
    head :no_content
  end

  # DELETE /todos/:id
  def destroy
    @schedule.destroy
    head :no_content
  end

  private

  def schedule_params
    # whitelist params
    params.permit(:name, :starts_at, :ends_at, :active_hours, :sunday, :monday, :tuesday, :wednesday, :thursday, :friday, :saturday)
  end

  def set_schedule
    @schedule = Schedule.find(params[:id])
  end
end
