module V1
    class EmployeesController < ApplicationController
        before_action :set_user, only: [:show, :update, :destroy]


    def index
        @employee= current_user.todos.paginate(page: params[:page], per_page: 20)
    json_response(@employee)
      end

      def create
        @employee = Employee.create!(employee_params)
        json_response(@employee, :created)
      end

      # GET /todos/:id
  def show
    json_response(@employee)
  end

  # PUT /todos/:id
  def update
    @employee.update(employee_params)
    head :no_content
  end

  # DELETE /todos/:id
  def destroy
    @employee.destroy
    head :no_content
  end

  private



  def set_employee
    @employee = Employee.find(params[:id])
  end
end

      def employee_params
        params.require(:employee).permit(:role, :user_id)
      end
  
    end
  