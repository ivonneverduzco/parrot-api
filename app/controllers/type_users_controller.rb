class TypeUsersController < ApplicationController
    
        before_action :set_type, only: [:show, :update, :destroy]
      
        # GET /todos
        def index
          @types_users = Type_user.all
          json_response(@types_users )
        end
      
        # POST /todos
        def create
          @todo = Type_user.create!(type_params)
          json_response(@type_user, :created)
        end
      
        # GET /todos/:id
        def show
          json_response(@type_user)
        end
      
        # PUT /todos/:id
        def update
          @type_user.update(type_params)
          head :no_content
        end
      
        # DELETE /todos/:id
        def destroy
          @type_user.destroy
          head :no_content
        end
      
        private
      
        def type_params
          # whitelist params
          params.permit(:type)
        end
      
        def set_type
          @type_users = Type_user.find(params[:id])
        end
end
      