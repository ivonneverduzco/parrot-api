class Employee < ApplicationRecord
  belongs_to :user
  validates_presence_of :role
end
