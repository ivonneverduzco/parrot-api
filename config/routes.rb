
  Rails.application.routes.draw do

    scope module: :v2, constraints: ApiVersion.new('v2') do
      resources :employees
      resources :type_users
      resources :shedules
    end
    
    scope module: :v1, constraints: ApiVersion.new('v1', true) do
      resources :employees 
      resources :type_users
      resources :shedules
      end

    post 'auth/login', to: 'authentication#authenticate'
    post 'signup', to: 'users#create'
    post '/push' => 'notifications#push'
    post '/message' => 'notifications#message'
  end
  
  
  
